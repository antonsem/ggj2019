﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenController : MonoBehaviour {

	public Transform mainCamera;
	
	public SpriteRenderer keyboardSprite;
	
	bool dismissed = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		if (Input.GetKeyDown(KeyCode.Space) && !dismissed)
		{
			dismissed = true;
			//keyboardSprite.enabled = false;
			StartCoroutine(LoadGameSceneCoroutine());
		}
	}
	
	IEnumerator LoadGameSceneCoroutine() {
		
		Vector3 camInitialPos = mainCamera.position;
		Vector3 camDestinationPos = mainCamera.position - Vector3.up * 5f;
		
		for (float t=0; t<=1; t+=Time.unscaledDeltaTime * 6f)
		{
			mainCamera.position = Vector3.Lerp(camInitialPos, camDestinationPos, t);
			
			keyboardSprite.color = Color.Lerp(keyboardSprite.color, new Color32(0,0,0,0), t);
			
			yield return null;
		}
		
		SceneManager.LoadScene("Game");
	}
}
