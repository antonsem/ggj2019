﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideSRInWorldState : MonoBehaviour {
	
	SpriteRenderer sr;
	
	void Awake() {
		sr = GetComponent<SpriteRenderer>();
	}
	
	void Update () {
		if (ObjectHolder.Instance.gameState.currentGameState == GameStateEnum.world)
			sr.enabled = false;
		else
			sr.enabled = true;
	}
}
