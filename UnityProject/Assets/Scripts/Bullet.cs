using System.Collections;
using UnityEngine;

public class Bullet : Hazard
{
    [SerializeField]
    private float speed = 10;
    public Vector2 dir;

    private WaitForSeconds wait = new WaitForSeconds(5);
    private IEnumerator coroutine;

    public override void Action(GameObject obj)
    {
        base.Action(obj);
        ResourceManager.Remove(gameObject);
    }

    private IEnumerator SelfDestroyCoroutine()
    {
        yield return wait;
        ResourceManager.Remove(gameObject);
        coroutine = null;
    }

    private void GameStateChanged(GameStateEnum gs)
    {
        ResourceManager.Remove(gameObject);
    }

    protected override void OnEnable()
    {
        if (coroutine == null)
            coroutine = SelfDestroyCoroutine();

        StartCoroutine(coroutine);

        Events.Instance.gameStateChanged.AddListener(GameStateChanged);
    }

    protected override void OnDisable()
    {
        StopCoroutine(coroutine);
        if (!isQuitting)
            Events.Instance.gameStateChanged.RemoveListener(GameStateChanged);
    }

    private void Update()
    {
        transform.Translate(dir * speed * Time.deltaTime);
    }
}