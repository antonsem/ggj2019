﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour {
	
	
	public float viewSpeed = 10f;
	
	Quaternion initialLocalRot;
	Quaternion desiredLocalRot;
	
	// Use this for initialization
	void Start () {
		initialLocalRot = transform.localRotation;
		desiredLocalRot = initialLocalRot;
		
		StartCoroutine(CameraFlyInCoroutine());
	}
	
	IEnumerator CameraFlyInCoroutine()
	{
		Vector3 destinationPos = transform.position;
		Vector3 initialPos = destinationPos + Vector3.up * 35f;
		
		transform.position = initialPos;
		
		for (float t=0; t<=1; t+=Time.unscaledDeltaTime * 1.5f)
		{
			transform.position = Vector3.Lerp(transform.position, destinationPos, t);
			
			yield return null;
		}
		
		transform.position = destinationPos;
	}
	
	// Update is called once per frame
	void Update () {
		
		
		transform.localRotation =
			Quaternion.Lerp(transform.localRotation, desiredLocalRot, Time.unscaledDeltaTime * viewSpeed);
		
		
		// inputs read only while space is held
		// world preview only in world mode
		if (!Input.GetKey(KeyCode.LeftControl) || ObjectHolder.Instance.gameState.currentGameState != GameStateEnum.world) {
			// reset view
			desiredLocalRot = initialLocalRot;
			return;
		}
		
		
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			desiredLocalRot = desiredLocalRot * Quaternion.Euler(0, 90, 0);
		}
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			desiredLocalRot = desiredLocalRot * Quaternion.Euler(0, -90, 0);
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			desiredLocalRot = desiredLocalRot * Quaternion.Euler(90, 0, 0);
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			desiredLocalRot = desiredLocalRot * Quaternion.Euler(-90, 0, 0);
		}

	}
}
