﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelContainer : MonoBehaviour {
	
	GameObject levelGO;
	GameObject levelPreviewGO;
	
	void Awake () {
		levelGO = transform.GetComponentInChildren<Level>(true).gameObject;
		levelPreviewGO = GetComponentInChildren<LevelPreview>().gameObject;
	}
	
	
	public void ActivateLevel()
	{
		levelGO.SetActive(true);
		levelPreviewGO.SetActive(false);
	}
	
	public void ActivatePreview()
	{
		levelGO.SetActive(false);
		levelPreviewGO.SetActive(true);
	}
}
