using UnityEngine;

public class TurretTile : MonoBehaviour
{
    [SerializeField]
    private Transform nozzle;
    [SerializeField]
    private float shootInterval = 1;
    private float timer = 1;

    private GameObject temp;

    private void Shoot()
    {
        temp = ResourceManager.Instance.fireball.Get();
        temp.transform.position = nozzle.position;
        temp.GetComponent<Bullet>().dir = nozzle.up;
    }

    private void Update()
    {
        if (ObjectHolder.Instance.gameState.currentGameState == GameStateEnum.world) return;
        if (timer > 0)
            timer -= Time.deltaTime;
        else
        {
            timer = shootInterval;
            Shoot();
        }
    }
}