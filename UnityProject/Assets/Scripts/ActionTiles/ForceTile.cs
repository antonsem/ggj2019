using ExtraTools;
using UnityEngine;

public class ForceTile : ActionTile
{
    [SerializeField]
    private float force = 15;
    [SerializeField]
    private GameObject toActivate;
    private PhysicsObject player;

    protected override void CheckActivation()
    {
        base.CheckActivation();
        toActivate.SetActive(active);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (!active) return;
        if (collision.attachedRigidbody && collision.attachedRigidbody.gameObject == ObjectHolder.Instance.Player.gameObject)
            collision.attachedRigidbody.transform.SetComponent(out player);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (player)
            player.targetVelocity = transform.up * force;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.attachedRigidbody && player && collision.attachedRigidbody.gameObject == player.gameObject)
            player = null;
    }
}