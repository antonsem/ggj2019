using ExtraTools;
using UnityEngine;

public class JumpTile : ActionTile
{
    [SerializeField]
    private float jumpForce = 20;
    [SerializeField]
    private float cooldown = 2;
    private float defaultCooldown = 2;

    private Player player;

    public override void Action(GameObject obj)
    {
        if (cooldown <= 0)
            cooldown = defaultCooldown;
        else return;

        if (obj.transform.SetComponent(out player))
            player.Jump(transform.up * jumpForce);
    }

    private void Start()
    {
        defaultCooldown = cooldown;
        cooldown = 0;
    }

    private void Update()
    {
        if (cooldown > 0)
            cooldown -= Time.deltaTime;
    }
}