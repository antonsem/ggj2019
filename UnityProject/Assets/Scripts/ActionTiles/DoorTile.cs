using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DoorTile : ActionTile
{

    [SerializeField]
    private Animator anim;

    private int stateHash = Animator.StringToHash("State");

    [SerializeField]
    private bool currentState = false;

    private void Start()
    {
        anim.SetBool(stateHash, currentState);
    }

    public override void Action(GameObject obj)
    {
        currentState = !currentState;

        anim.SetBool(stateHash, currentState);
    }

#if UNITY_EDITOR
    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }

    [Space(10), Header("Inspector"), Space(5), SerializeField, InspectorButton("Toggle")]
    private string toggle;

    private void Toggle()
    {
        Action(null);
    }
#endif
}