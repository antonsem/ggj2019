using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PressureTile : ActionTile
{
    [SerializeField]
    private ActionTile[] tiles;
    [SerializeField]
    private Animator anim;

    private int pressHash = Animator.StringToHash("Press");

    public override void Action(GameObject obj)
    {
        anim.SetTrigger(pressHash);

        if(tiles != null && tiles.Length > 0)
        {

            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].Action(obj);
            }
        }
        
        SoundManager.Instance.Play(SoundManager.soundId.tilePressure);
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }
}