using UnityEngine;

public class ActionTile : MonoBehaviour
{
    [SerializeField]
    protected bool active = true;
    [SerializeField]
    private Pickup dependantOn = Pickup.None;
    protected GameState state;

    protected bool isQuitting = false;

    protected virtual void OnEnable()
    {
        Events.Instance.dropepdPickup.AddListener((val) => CheckActivation());
    }

    protected virtual void OnDisable()
    {
        if (!isQuitting)
            Events.Instance.dropepdPickup.RemoveListener((val) => CheckActivation());
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    protected virtual void Start()
    {
        state = ObjectHolder.Instance.gameState;
        CheckActivation();
    }

    protected virtual void CheckActivation()
    {
        active = dependantOn == Pickup.None || state.collection[dependantOn];
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (!active) return;
        if (collision.attachedRigidbody)
            Action(collision.attachedRigidbody.gameObject);
    }

    public virtual void Action(GameObject obj)
    {

    }
}