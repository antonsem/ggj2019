using UnityEngine;

public enum Pickup
{
    None,
    Square,
    Triangle,
    Rombus,
    Star,
    Pentagram
}

public class PickupTile : ActionTile
{
    public Pickup type;
    public Collider2D col;
    
    private GameObject createdMarker;
    
    public override void Action(GameObject obj)
    {
        if (obj == ObjectHolder.Instance.Player.gameObject
            && ObjectHolder.Instance.Player.tile == null)
        {
            ObjectHolder.Instance.Player.tile = this;
            transform.SetParent(obj.transform, false);
            transform.localPosition = Vector2.zero;
            enabled = false;
            col.enabled = false;
            Events.Instance.gotPickup.Invoke(this);
            GetComponentInChildren<SpriteRenderer>().gameObject.AddComponent<HideSRInWorldState>();
            SoundManager.Instance.Play(SoundManager.soundId.tilePickup);
            //ResourceManager.Remove(createdMarker);
        }
    }
    
    void Awake()
    {
        // create world-state marker
        createdMarker = ResourceManager.Instance.worldStateMarker.Get();
        createdMarker.GetComponent<WorldStateMarker>().objectToFollow = transform;
        createdMarker.GetComponentInChildren<SpriteRenderer>().sprite = 
            gameObject.GetComponentInChildren<SpriteRenderer>().sprite;
        createdMarker.transform.localScale *= 2;
    }
}