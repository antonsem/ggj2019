using UnityEngine;

public class DropTile : ActionTile
{
    [SerializeField]
    private Pickup type = Pickup.Square;

    public override void Action(GameObject obj)
    {
        if(obj == ObjectHolder.Instance.Player.gameObject 
            && ObjectHolder.Instance.Player.tile
            && ObjectHolder.Instance.Player.tile.type == type)
        {
            ObjectHolder.Instance.Player.tile.transform.SetParent(transform, false);
            ObjectHolder.Instance.Player.tile.transform.localPosition = Vector2.zero;
            enabled = false;
            state.GotPickup(ObjectHolder.Instance.Player.tile);
            ObjectHolder.Instance.Player.tile = null;
            SoundManager.Instance.Play(SoundManager.soundId.tileDrop);
        }

    }
    
    
    void Awake()
    {
        // create world-state marker
        GameObject createdMarker = ResourceManager.Instance.worldStateMarker.Get();
        createdMarker.GetComponent<WorldStateMarker>().objectToFollow = transform;
        createdMarker.GetComponentInChildren<SpriteRenderer>().sprite = 
            gameObject.GetComponentInChildren<SpriteRenderer>().sprite;
        createdMarker.transform.localScale *= 2;
    }
}