﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldStateMarker : MonoBehaviour {
	
	[HideInInspector]
	public Transform objectToFollow;
	
	SpriteRenderer sr;
	
	void Awake() {
		sr = GetComponentInChildren<SpriteRenderer>();
	}
	
	void Update () {
		
		if (!objectToFollow) return;
		
		transform.position = objectToFollow.position;
		transform.rotation = objectToFollow.rotation;
		
		if (ObjectHolder.Instance.gameState.currentGameState != GameStateEnum.world)
			sr.enabled = false;
		else
			sr.enabled = true;
	}
}
