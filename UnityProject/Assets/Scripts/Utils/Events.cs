using UnityEngine.Events;

public class EventEmmiter : UnityEvent
{ }

public class LevelEmmiter : UnityEvent<Level>
{ }

public class CheckpointEmmiter : UnityEvent<Checkpoint>
{ }

public class PickupEmmiter : UnityEvent<PickupTile>
{ }

public class PlayerStateEmmiter : UnityEvent<PlayerState>
{ }

public class GameStateEmmiter : UnityEvent<GameStateEnum>
{ }

public class Events : Singleton<Events>
{
    public LevelEmmiter levelEntered = new LevelEmmiter();
    public EventEmmiter playerPhysicsUpdated = new EventEmmiter();
    public CheckpointEmmiter checkpointEntered = new CheckpointEmmiter();
    public PickupEmmiter gotPickup = new PickupEmmiter();
    public PickupEmmiter dropepdPickup = new PickupEmmiter();
    public PlayerStateEmmiter playerStateChanged = new PlayerStateEmmiter();
    public GameStateEmmiter gameStateChanged = new GameStateEmmiter();
}
