using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ResourceObject
{
    public GameObject prefab;

    private List<GameObject> objects = new List<GameObject>();

    public GameObject Get()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            if(!objects[i].activeSelf)
            {
                objects[i].SetActive(true);
                return objects[i];
            }
        }

        objects.Add(UnityEngine.Object.Instantiate(prefab));
        return objects[objects.Count - 1];
    }
}

public class ResourceManager : Singleton<ResourceManager>
{
    public ResourceObject fireball;
    public ResourceObject worldStateMarker;
    public ResourceObject playerExplosion;

    private static Transform root;

    private void Awake()
    {
        root = new GameObject("Root").transform;
    }

    public static void Remove(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.SetParent(root);
    }
}