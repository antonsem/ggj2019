﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class soundGroup
{
	public AudioClip cubeTurn;
	public AudioClip viewSwitch;
	
	public AudioClip tilePickup;
	public AudioClip tileDrop;
	public AudioClip tilePressure;
	
	public AudioClip playerDead;
	public AudioClip playerGlued;
	public AudioClip playerUnglued;
}

public class SoundManager : Singleton<SoundManager> {
	
	public enum soundId : int
	{
		cubeTurn,
		viewSwitch,
		
		tilePickup,
		tileDrop,
		tilePressure,
		
		playerDead,
		playerGlued,
		playerUnglued
	};
	
	AudioSource audioSrc; // global audio source
	
	// sound file references
	[Header("Assets")]
	public soundGroup gameSounds;
	
	
	void Awake()
	{
		audioSrc = gameObject.GetComponent<AudioSource>();
	}
	
	
	public void Play(soundId id, float volume = 1)
	{
		switch (id)
		{
			// world
			case soundId.cubeTurn:
				audioSrc.PlayOneShot(gameSounds.cubeTurn, volume);
				break;
			case soundId.viewSwitch:
				audioSrc.PlayOneShot(gameSounds.viewSwitch, volume);
				break;
			
			// level
			case soundId.tilePickup:
				audioSrc.PlayOneShot(gameSounds.tilePickup, volume);
				break;
			case soundId.tileDrop:
				audioSrc.PlayOneShot(gameSounds.tileDrop, volume);
				break;
			case soundId.tilePressure:
				audioSrc.PlayOneShot(gameSounds.tilePressure, volume);
				break;
				
			case soundId.playerDead:
				audioSrc.PlayOneShot(gameSounds.playerDead, volume);
				break;
			case soundId.playerGlued:
				audioSrc.PlayOneShot(gameSounds.playerGlued, volume);
				break;
			case soundId.playerUnglued:
				audioSrc.PlayOneShot(gameSounds.playerUnglued, volume);
				break;
		}
	}
	
}
