﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour {

	public GameObject playerDeadPanel;
	
	void Start () {
		Events.Instance.playerStateChanged.AddListener(PlayerStateChangedCallback);
		
		
		playerDeadPanel.SetActive(false);
	}
	
	void PlayerStateChangedCallback(PlayerState playerState)
	{
		switch(ObjectHolder.Instance.gameState.PlayerState)
		{
			case PlayerState.Alive:
				playerDeadPanel.SetActive(false);
				break;
			
			case PlayerState.Dead:
				playerDeadPanel.SetActive(true);
				break;
		}
	}
}
