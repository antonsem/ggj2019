using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Glue : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D rigid;
    [SerializeField]
    private Collider2D[] colliders;
    public bool Sticking { get; private set; } = false;

    public void SetState(bool state)
    {
        if (!state)
            Sticking = false;
        if (colliders == null)
        {
            Debug.LogError(string.Format("Colliders are not set on {0}!", name));
            return;
        }

        for (int i = 0; i < colliders.Length; i++)
            colliders[i].enabled = state;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Sticking = true;
        //SoundManager.Instance.Play(SoundManager.soundId.playerGlued);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Sticking = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Sticking = false;
    }

    private void Reset()
    {
        transform.SetComponent(out rigid);
    }
}
