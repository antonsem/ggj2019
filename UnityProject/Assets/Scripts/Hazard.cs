using UnityEngine;

public class Hazard : ActionTile
{
    public override void Action(GameObject obj)
    {
        if (obj == ObjectHolder.Instance.Player.gameObject)
            ObjectHolder.Instance.Player.Die();
    }
}