﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewSwitch : Singleton<ViewSwitch> {
	
	[Space]
	public CameraController cameraController;
	public LevelActivator levelActivator;
	public Cubehub cubehub;
	
	[Space]
	public Music music;
	
	void LateUpdate () {
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (ObjectHolder.Instance.gameState.PlayerState != PlayerState.Alive) return;
			
			switch (ObjectHolder.Instance.gameState.currentGameState)
			{
				case GameStateEnum.world:
					ObjectHolder.Instance.gameState.currentGameState = GameStateEnum.level;
					
					levelActivator.ActivateFrontfacingLevelTiles();
					cameraController.SwitchCameraMode(CameraMode.level);
					ObjectHolder.Instance.Player.Unfreeze();
					break;
				
				case GameStateEnum.level:
					ObjectHolder.Instance.gameState.currentGameState = GameStateEnum.world;
					
					cubehub.DeactivateAllLevelTiles();
					cameraController.SwitchCameraMode(CameraMode.world);
					ObjectHolder.Instance.Player.Freeze();
					break;
			}
			
			SoundManager.Instance.Play(SoundManager.soundId.viewSwitch);
			
			music.SwitchEffects();
		}
	}
}
