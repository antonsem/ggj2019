using System.Collections.Generic;
using UnityEngine;

public enum GameStateEnum
{
    world,
    level
}

public enum PlayerState
{
    Alive,
    Dead
}

[CreateAssetMenu(fileName = "GameState", menuName = "qbk/Game State")]
public class GameState : ScriptableObject
{
    public GameStateEnum initialGameState;
    private GameStateEnum _currentGameState;
    [HideInInspector]
    public GameStateEnum currentGameState
    {
        get { return _currentGameState; }
        set
        {
            if(_currentGameState != value)
            {
                _currentGameState = value;
                Events.Instance.gameStateChanged.Invoke(value);
            }
        }
    }

    [SerializeField]
    private PlayerState playerState = PlayerState.Alive;
    public PlayerState PlayerState
    {
        get { return playerState; }
        set
        {
            if(playerState != value)
            {
                playerState = value;
                Events.Instance.playerStateChanged.Invoke(value);
            }
        }
    }
    
    public Dictionary<Pickup, bool> collection = new Dictionary<Pickup, bool>()
    {
        { Pickup.Pentagram, false },
        { Pickup.Rombus, false },
        { Pickup.Square, false },
        { Pickup.Star, false },
        { Pickup.Triangle, false }
    };

    private void OnEnable()
    {
        currentGameState = initialGameState;
        
        if (Application.isPlaying)
            foreach (KeyValuePair<Pickup, bool> pair in collection)
                collection[pair.Key] = false;
    }

    public void GotPickup(PickupTile obj)
    {
        if (obj.type != Pickup.None)
            collection[obj.type] = true;

        Events.Instance.dropepdPickup.Invoke(obj);
    }
}