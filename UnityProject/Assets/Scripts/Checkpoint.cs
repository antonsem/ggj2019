using System.Collections;
using UnityEngine;

public class Checkpoint : ActionTile
{
    public GameObject flag;

    private IEnumerator Enable()
    {
        yield return null;
        flag.SetActive(LevelController.Instance.CurrentLevel.GetCurrentCheckpoint() == this);
        Events.Instance.checkpointEntered.AddListener(SetFlag);
    }

    protected override void OnEnable()
    {
        StartCoroutine(Enable());
    }

    protected override void OnDisable()
    {
        flag.SetActive(false);
        if (!isQuitting)
            Events.Instance.checkpointEntered.RemoveListener(SetFlag);
    }

    private void SetFlag(Checkpoint cp)
    {
        flag.SetActive(cp == this);
    }

    public override void Action(GameObject obj)
    {
        Events.Instance.checkpointEntered.Invoke(this);
    }

}