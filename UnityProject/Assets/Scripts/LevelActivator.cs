﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelActivator : MonoBehaviour {

	public LayerMask raycastLayer;
	public Transform[] raySource;

	/*void Update () {
		
		if (Input.GetKeyDown(KeyCode.Q))
		{
			ActivateFrontfacingLevelTiles();
		}
	}*/
	
	
	
	public void ActivateFrontfacingLevelTiles()
	{
		foreach(Transform t in raySource)
		{
			Debug.DrawLine(t.position, t.position + Vector3.forward * Mathf.Abs(transform.position.z), Color.red, 0.1f);
			
			RaycastHit hit;
			Ray ray = new Ray(t.position, Vector3.forward);
			float rayDistance = Mathf.Abs(transform.position.z) + 8f;
			if (Physics.Raycast(ray, out hit, rayDistance, raycastLayer))
			{
				if (hit.collider.isTrigger)
				{
					//Debug.Log("hit " + hit.collider.gameObject.name);
					
					hit.collider.gameObject.GetComponent<LevelContainer>().ActivateLevel();
				}
			}
		}
	}
}
