﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {
	
	AudioHighPassFilter hpf;
	AudioReverbFilter rvb;
	
	void Awake () {
		hpf = GetComponent<AudioHighPassFilter>();
		rvb = GetComponent<AudioReverbFilter>();
	}
	
	
	public void SwitchEffects()
	{
		switch (ObjectHolder.Instance.gameState.currentGameState)
		{
			case GameStateEnum.world:
				hpf.enabled = true;
				rvb.enabled = true;
				break;
			
			case GameStateEnum.level:
				hpf.enabled = false;
				rvb.enabled = false;
				break;
		}
	}
}
