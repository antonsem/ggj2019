﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wrapper : MonoBehaviour {
	
	public bool playerPresent;
	
	[Header("Coords in world")]
	public int row;
	public int column;
	public int layer;
	
	
	Cubehub cubehub;
	private LevelContainer[] wrappedLevelContainer;
	
	string nameBase;
	//Transform innerCube;
	
	void Awake()
	{
		cubehub = GetComponentInParent<Cubehub>();
		wrappedLevelContainer = GetComponentsInChildren<LevelContainer>();
		
		nameBase = gameObject.name.Substring(0, gameObject.name.Length - 3);
		//innerCube = GetComponentInChildren<MeshRenderer>().transform;
	}
	
	
	/*void Start()
	{
		StartCoroutine(FlyInCoroutine());
	}
	IEnumerator FlyInCoroutine() {
		
		Vector3 startPosition = innerCube.localPosition * 5f;
		
		for (float t = 0; t <= 1; t+=Time.unscaledDeltaTime * 1f)
		{
			transform.localPosition = Vector3.Lerp(startPosition, Vector3.zero, t);
			yield return null;
		}
		transform.localPosition = Vector3.zero;
	}*/
	
	public void TurnAround(int horizDir, int vertDir)
	{
		
		StartCoroutine(TurnAroundCoroutine(horizDir, vertDir, cubehub.worldRotationSpeed));
		
		UpdateWorldCoords(horizDir, vertDir, cubehub.worldSize);
		
		// TODO
		// rotation sound
		// rotation timer?
	}
	IEnumerator TurnAroundCoroutine(int horizDir, int vertDir, float speed)
	{
		/*Quaternion initialRot = transform.rotation;
		Quaternion desiredRot = initialRot * Quaternion.Euler(vertDir * 90, horizDir * 90, 0);
		
		Debug.Log(initialRot.eulerAngles + " > " + desiredRot.eulerAngles);
		
		for (float t = 0; t <= 1; t+= Time.unscaledDeltaTime * speed)
		{
			transform.rotation = Quaternion.Lerp(initialRot, desiredRot, t);
			yield return null;
		}
		transform.rotation = desiredRot;*/
		
		const float stepDivider = 0.1f;
		Vector3 rotationVector = new Vector3(vertDir * 90, horizDir * 90, 0);
		for (float p = 0; p <= 1; p += stepDivider)
		{
			transform.Rotate(Vector3.Lerp(Vector3.zero, rotationVector, stepDivider), Space.World);
			yield return new WaitForSecondsRealtime(speed);
		}
		
		cubehub.turnAroundInProgress = false;
	}
	
	void UpdateWorldCoords(int horizDir, int vertDir, int worldSize)
	{
		int newRow = -1, newColumn = -1, newLayer = -1;
		
		switch(horizDir)
		{
			case 1:
				//Debug.Log(gameObject.name + " turning left");
				newRow = row;
				newColumn = layer;
				newLayer = (worldSize - 1) - column;
				break;
			
			case -1:
				//Debug.Log(gameObject.name + " turning right");
				newRow = row;
				newColumn = (worldSize - 1) - layer;
				newLayer = column;
				break;
		}
		switch(vertDir)
		{
			case 1:
				//Debug.Log(gameObject.name + " turning up");
				newRow = layer;
				newColumn = column;
				newLayer = (worldSize - 1) - row;
				break;
			case -1:
				//Debug.Log(gameObject.name + " turning down");
				newRow = (worldSize - 1) - layer;
				newColumn = column;
				newLayer = row;
				break;
		}
		
		row = newRow;
		column = newColumn;
		layer = newLayer;
		
		gameObject.name = nameBase + row.ToString() + column.ToString() + layer.ToString();
	}
	
	
	
	public void DeactivateAllWrappedLevels()
	{
		foreach(LevelContainer levCont in wrappedLevelContainer)
		{
			levCont.ActivatePreview();
		}
	}
	
	public void SetPlayerNotPresent() {
		playerPresent = false;
	}
	public void SetPlayerPresentJustHere()
	{
		// false everywhere else
		cubehub.MarkAllWrappersAsPlayerNotBeingPresent();
		
		// present here
		playerPresent = true;
	}
}
