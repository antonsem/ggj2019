using System.Collections;
using UnityEngine;

public class DisableSelf : MonoBehaviour
{
    [SerializeField]
    private float disableAfter = 10;

    private IEnumerator disable;

    private void OnEnable()
    {
        if (disable == null)
            disable = Disable();

        StartCoroutine(disable);
    }

    private void OnDisable()
    {
        StopCoroutine(disable);
    }

    private IEnumerator Disable()
    {
        yield return new WaitForSeconds(disableAfter);
        ResourceManager.Remove(gameObject);
    }
}