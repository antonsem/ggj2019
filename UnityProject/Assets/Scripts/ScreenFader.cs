﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFader : MonoBehaviour {
	
	
	SpriteRenderer sr;
	
	void Awake () {
		sr = GetComponent<SpriteRenderer>();
	}
	
	void Start () {
		StartCoroutine(FadeScreenIn());
	}
	
	IEnumerator FadeScreenIn()
	{
		sr.enabled = true;
		Color initialColor = sr.color;
		Color transparent = new Color(0, 0, 0, 0);
		
		for (float t=0; t<=1; t+=Time.unscaledDeltaTime * 0.5f)
		{
			sr.color = Color.Lerp(initialColor, transparent, t);
			yield return null;
		}
		
		sr.enabled = false;
	}
}
